module gitlab.com/tcnj/acme-manager

require (
	github.com/caddyserver/certmagic v0.12.0
	github.com/fatih/color v1.9.0 // indirect
	github.com/go-test/deep v1.0.7 // indirect
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/google/uuid v1.2.0
	github.com/hashicorp/go-retryablehttp v0.6.7 // indirect
	github.com/hashicorp/hcl v1.0.1-vault // indirect
	github.com/hashicorp/vault/api v1.0.5-0.20201001211907-38d91b749c77
	github.com/hashicorp/vault/sdk v0.1.14-0.20210127182440-8477cfe632c0 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/libdns/cloudflare v0.0.0-20200528144945-97886e7873b1
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pkg/sftp v1.12.0
	go.uber.org/zap v1.15.0
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0
	golang.org/x/sys v0.0.0-20200831180312-196b9ba8737a // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)

go 1.15
