package config

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/caddyserver/certmagic"

	"github.com/libdns/cloudflare"
)

func buildProvider(name string, rawConfig json.RawMessage) (certmagic.ACMEDNSProvider, error) {
	var err error
	var provider certmagic.ACMEDNSProvider

	switch name {
	case "cloudflare":
		var config cloudflare.Provider
		err = json.Unmarshal(rawConfig, &config)
		provider = &config

		if config.APIToken == "" {
			log.Fatal("expected api_token for cloudflare provider")
		}
	default:
		return nil, fmt.Errorf("unknown provider name %s", name)
	}

	if err != nil {
		return nil, err
	}

	return provider, nil
}
