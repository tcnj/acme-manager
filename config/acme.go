package config

import (
	"encoding/json"

	"github.com/caddyserver/certmagic"
)

// ACMEConfig represents acme-related configuration
type ACMEConfig struct {
	Directory string `json:"directory"`
	Email     string `json:"email"`
	Agreed    bool   `json:"agreed"`

	DisableHTTPChallenge bool              `json:"disable_http"`
	DNSProvider          DNSProviderConfig `json:"dns_provider"`

	Listen ACMEListenConfig
}

// DNSProviderConfig represents the config for a DNS Provider
type DNSProviderConfig struct {
	Name string `json:"name"`

	Settings json.RawMessage `json:"settings"`

	provider certmagic.ACMEDNSProvider
}

// GetSolver returns a solver based on this config
func (c *DNSProviderConfig) GetSolver() *certmagic.DNS01Solver {
	if c.provider == nil {
		return nil
	}

	return &certmagic.DNS01Solver{
		DNSProvider: c.provider,
	}
}

// ACMEListenConfig represents the listener config
type ACMEListenConfig struct {
	Host string `json:"host"`

	HTTPPort int `json:"http_port"`
}
