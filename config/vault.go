package config

// VaultConfig represents the vault-related portion of the config
type VaultConfig struct {
	Address string `json:"address"`
}
