package config

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"io/ioutil"
	"log"
	"os"

	vault "github.com/hashicorp/vault/api"
	"gitlab.com/tcnj/acme-manager/storage"
)

var configPathFlag string

func init() {
	flag.StringVar(&configPathFlag, "config", "./acme_manager.json", "Configuration file path")
}

// Config represents acme_manager's configuration as loaded from disk
type Config struct {
	Vault   VaultConfig  `json:"vault"`
	Storage storage.Spec `json:"storage"`

	ACME ACMEConfig `json:"acme"`

	Certificates []CertConfig `json:"certificates"`
}

// DefaultConfig loads a config from the default locations
func DefaultConfig() (*Config, error) {
	return LoadConfig(configPathFlag)
}

// LoadConfig loads a Config from path
func LoadConfig(path string) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	config := Config{
		ACME: ACMEConfig{
			Directory:            "https://acme-staging-v02.api.letsencrypt.org/directory",
			DisableHTTPChallenge: true,
		},
	}
	err = json.Unmarshal(bytes, &config)
	if err != nil {
		return nil, err
	}

	if len(config.Certificates) == 0 {
		log.Fatal("At least one certificate is required")
	}

	if config.ACME.DNSProvider.Name != "" {
		name := config.ACME.DNSProvider.Name
		provider, err := buildProvider(name, config.ACME.DNSProvider.Settings)
		if err != nil {
			return nil, err
		}

		config.ACME.DNSProvider.provider = provider
	}

	if config.ACME.DNSProvider.provider == nil && config.ACME.DisableHTTPChallenge {
		return nil, errors.New("either a DNS provider or HTTP challenges should be enabled")
	}

	return &config, nil
}

// BuildVaultClient builds a vault client from the config
func (c *Config) BuildVaultClient() (*vault.Client, error) {
	vaultConfig := vault.DefaultConfig()
	vaultConfig.Address = c.Vault.Address

	return vault.NewClient(vaultConfig)
}

type configKey int

// WrapContext wraps this config into the given context
func (c *Config) WrapContext(ctx context.Context) context.Context {
	return context.WithValue(ctx, configKey(0), c)
}

// FromContext takes a Config from a context
func FromContext(ctx context.Context) *Config {
	c, ok := ctx.Value(configKey(0)).(*Config)
	if !ok {
		panic("tried to take a config from a context where one didn't exist")
	}
	return c
}
