package config

import "gitlab.com/tcnj/acme-manager/installer"

// CertConfig represents the config for a single certificate
type CertConfig struct {
	Domain string `json:"domain"`

	Installers []installer.Spec `json:"installers"`
}
