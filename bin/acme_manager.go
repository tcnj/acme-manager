package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/caddyserver/certmagic"
	"go.uber.org/zap"

	"gitlab.com/tcnj/acme-manager/config"
	"gitlab.com/tcnj/acme-manager/convert"
	"gitlab.com/tcnj/acme-manager/installer"
	"gitlab.com/tcnj/acme-manager/storage"
	"gitlab.com/tcnj/acme-manager/vault"
)

func createMagic(ctx context.Context) (*certmagic.Config, error) {
	conf := config.FromContext(ctx)

	storage, err := storage.FromSpec(ctx, &conf.Storage)
	if err != nil {
		return nil, err
	}

	magic := certmagic.NewDefault()
	magic.Storage = storage

	logger, err := zap.NewDevelopment()
	if err != nil {
		return nil, err
	}
	magic.Logger = logger

	acmeHandler := certmagic.NewACMEManager(magic, certmagic.ACMEManager{
		Agreed:                  conf.ACME.Agreed,
		CA:                      conf.ACME.Directory,
		AltHTTPPort:             conf.ACME.Listen.HTTPPort,
		DisableHTTPChallenge:    conf.ACME.DisableHTTPChallenge,
		DisableTLSALPNChallenge: true,
		DNS01Solver:             conf.ACME.DNSProvider.GetSolver(),
		Email:                   conf.ACME.Email,
		ListenHost:              conf.ACME.Listen.Host,
	})

	magic.Issuer = acmeHandler

	return magic, nil
}

func main() {
	flag.Parse()

	conf, err := config.DefaultConfig()
	if err != nil {
		log.Fatal(err)
	}

	client, err := conf.BuildVaultClient()
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()
	ctx = vault.WrapContext(ctx, client)
	ctx = conf.WrapContext(ctx)

	go (func() {
		address := fmt.Sprintf("%s:%d", conf.ACME.Listen.Host, conf.ACME.Listen.HTTPPort)

		err := http.ListenAndServe(address, http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			certmagic.DefaultACME.HandleHTTPChallenge(rw, r)
		}))

		log.Fatal(err)
	})()

	for i := 0; i < len(conf.Certificates); i++ {
		certConfig := conf.Certificates[i]

		magic, err := createMagic(ctx)
		if err != nil {
			log.Fatal(err)
		}

		installers := []installer.Installer{}
		for j := 0; j < len(certConfig.Installers); j++ {
			i, err := installer.FromSpec(&certConfig.Installers[j])
			if err != nil {
				log.Fatal(err)
			}

			installers = append(installers, i)
		}

		go (func() {
			for {
				log.Print("Managing ", certConfig.Domain, "...")
				err := magic.ManageSync([]string{certConfig.Domain})
				if err != nil {
					log.Print("Error managing certificate for domain ", certConfig.Domain, ": ", err)
					log.Print("Trying again in 5 minutes")
					time.Sleep(time.Minute * 5)
					continue
				}

				var cert certmagic.Certificate
				waitTime := 1
				for {
					cert, err = magic.CacheManagedCertificate(certConfig.Domain)
					if err != nil {
						log.Print("error fetching certificate from storage: ", err, ", sleeping for ", waitTime, " seconds")
						time.Sleep(time.Second * time.Duration(waitTime))

						if waitTime < 60 {
							waitTime = waitTime * 2
						}
					} else {
						break
					}
				}

				log.Print("Successfully obtained certificate!")
				certPem, err := convert.TLSToPem(certConfig.Domain, &cert.Certificate)
				if err != nil {
					log.Fatal(err)
				}
				log.Print(certPem.Cert)

				installersDone := make(chan error)

				for j := 0; j < len(installers); j++ {
					installer := installers[j]
					installerName := certConfig.Installers[j].Type
					index := j
					go func() {
						err = installer.Install(ctx, certPem)

						if err != nil {
							log.Printf("whilst trying to install certificate for %s via %s/%d: %v", certConfig.Domain, installerName, index, err)
						} else {
							log.Printf("successfully installed certificate for %s via %s/%d", certConfig.Domain, installerName, index)
						}

						installersDone <- err
					}()
				}

				anyErrors := false
				for j := 0; j < len(installers); j++ {
					if <-installersDone != nil {
						anyErrors = true
					}
				}
				log.Printf("all installers for %s are complete", certConfig.Domain)

				if anyErrors {
					log.Print("some installers errored, trying again in 5 minutes")
					time.Sleep(time.Minute * 5)
				} else {
					time.Sleep(time.Hour * 24 * 30)
				}
			}
		})()
	}

	never := make(chan bool)
	<-never
}
