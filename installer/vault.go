package installer

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/tcnj/acme-manager/convert"
	"gitlab.com/tcnj/acme-manager/util"
	"gitlab.com/tcnj/acme-manager/vault"
)

// VaultInstaller stores a certificate in Vault
type VaultInstaller struct {
	KVEnginePath string `json:"kv_engine_path"`
}

// NewVault creates a VaultInstaller
func NewVault(settings json.RawMessage) (*VaultInstaller, error) {
	var i VaultInstaller

	err := json.Unmarshal(settings, &i)
	if err != nil {
		return nil, err
	}

	if i.KVEnginePath == "" {
		return nil, fmt.Errorf("expected kv_engine_path")
	}

	return &i, nil
}

// Install installs the certificate
func (i *VaultInstaller) Install(ctx context.Context, cert *convert.PemCertChain) error {
	client := vault.FromContext(ctx)
	destPath := i.pathForCertificateName(ctx, cert.Domain)

	return util.NewBackoff().WithCancel().WithLogger(func(err error, d time.Duration) {
		log.Printf(
			"encountered the following whilst trying to install %s to vault: %v, backing off for %v",
			cert.Domain,
			err,
			d,
		)
	}).Run(func() error {
		_, err := client.Logical().Write(destPath, map[string]interface{}{
			"data": cert,
		})

		return err
	})
}

func (i *VaultInstaller) pathForCertificateName(ctx context.Context, name string) string {
	canonical := convert.CanonicaliseDNSName(name)

	return strings.Join([]string{i.KVEnginePath, "data", "issued", canonical}, "/")
}
