package installer

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/tcnj/acme-manager/convert"
)

// Installer represents a type that can install a certificate somewhere
type Installer interface {
	Install(ctx context.Context, cert *convert.PemCertChain) error
}

// Spec represents an installer config
type Spec struct {
	Type     string          `json:"type"`
	Settings json.RawMessage `json:"settings"`
}

// FromSpec builds an Installer from a Spec
func FromSpec(spec *Spec) (Installer, error) {
	var err error
	var installer Installer

	switch spec.Type {
	case "sftp":
		installer, err = NewSFTP(spec.Settings)
	case "vault":
		installer, err = NewVault(spec.Settings)
	case "local":
		installer, err = NewLocal(spec.Settings)
	default:
		return nil, fmt.Errorf("unknown installer type %s", spec.Type)
	}

	if err != nil {
		return nil, err
	}

	return installer, nil
}
