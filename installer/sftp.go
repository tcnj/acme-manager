package installer

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"
	"time"

	"github.com/pkg/sftp"
	"gitlab.com/tcnj/acme-manager/convert"
	"gitlab.com/tcnj/acme-manager/hook"
	"gitlab.com/tcnj/acme-manager/util"
	"golang.org/x/crypto/ssh"
)

// SFTPInstaller installs a certificate over SFTP
type SFTPInstaller struct {
	Address string `json:"address"`
	User    string `json:"user"`

	Password       string `json:"password"`
	PrivateKeyPem  string `json:"private_key_pem"`
	PrivateKeyFile string `json:"private_key_file"`

	KnownHostsFile   string `json:"known_hosts_file"`
	KnownHostsInline string `json:"known_hosts_inline"`

	Files map[string]*convert.CertFileConfig `json:"files"`

	PreCopyHooks  []hook.Hook `json:"pre_copy_hooks"`
	PostCopyHooks []hook.Hook `json:"post_copy_hooks"`

	knownHosts map[string]ssh.PublicKey
}

// NewSFTP creates a new SFTP installer
func NewSFTP(settings json.RawMessage) (*SFTPInstaller, error) {
	var i SFTPInstaller

	err := json.Unmarshal(settings, &i)
	if err != nil {
		return nil, err
	}

	if !strings.Contains(i.Address, ":") {
		i.Address = i.Address + ":22"
	}

	i.knownHosts = nil
	if i.KnownHostsInline != "" || i.KnownHostsFile != "" {
		i.knownHosts = make(map[string]ssh.PublicKey)

		if i.KnownHostsInline != "" {
			err = i.parseKnownHosts([]byte(i.KnownHostsInline))
			if err != nil {
				return nil, err
			}
		}

		if i.KnownHostsFile != "" {
			file, err := os.Open(i.KnownHostsFile)
			if err != nil {
				return nil, err
			}

			defer file.Close()

			bytes, err := ioutil.ReadAll(file)
			if err != nil {
				return nil, err
			}

			err = i.parseKnownHosts(bytes)
			if err != nil {
				return nil, err
			}
		}
	}

	client, err := i.dialClient()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	session, err := client.NewSession()
	if err != nil {
		return nil, err
	}
	defer session.Close()

	outputChan := make(chan string)
	errChan := make(chan error)

	go func() {
		out, err := session.StdoutPipe()
		if err != nil {
			errChan <- err
			return
		}

		bytes, err := ioutil.ReadAll(out)
		if err != nil {
			errChan <- err
			return
		}

		outputChan <- string(bytes)
	}()

	err = session.Run("echo Hi!")
	if err != nil {
		return nil, fmt.Errorf("ssh run error: %v", err)
	}

	select {
	case output := <-outputChan:
		if output != "Hi!\n" {
			return nil, fmt.Errorf("expected test string but received %s", output)
		}
	case err = <-errChan:
		return nil, err
	}

	for _, conf := range i.Files {
		err = conf.Validate()
		if err != nil {
			return nil, err
		}
	}

	if len(i.Files) == 0 {
		return nil, fmt.Errorf("expected at least one file to write")
	}

	return &i, nil
}

func (i *SFTPInstaller) dialClient() (*ssh.Client, error) {
	authMethods := []ssh.AuthMethod{}

	if i.Password != "" {
		authMethods = append(authMethods, ssh.Password(i.Password))
	}

	if i.PrivateKeyPem != "" || i.PrivateKeyFile != "" {
		signers := []ssh.Signer{}

		if i.PrivateKeyPem != "" {
			signer, err := ssh.ParsePrivateKey([]byte(i.PrivateKeyPem))
			if err != nil {
				return nil, err
			}

			signers = append(signers, signer)
		}

		if i.PrivateKeyFile != "" {
			file, err := os.Open(i.PrivateKeyFile)
			if err != nil {
				return nil, err
			}

			defer file.Close()

			bytes, err := ioutil.ReadAll(file)
			if err != nil {
				return nil, err
			}

			signer, err := ssh.ParsePrivateKey(bytes)
			if err != nil {
				return nil, err
			}

			signers = append(signers, signer)
		}

		authMethods = append(authMethods, ssh.PublicKeys(signers...))
	}

	client, err := ssh.Dial("tcp", i.Address, &ssh.ClientConfig{
		User: i.User,
		Auth: authMethods,
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			if i.knownHosts != nil {
				targetKey, ok := i.knownHosts[hostname]

				if !ok {
					return fmt.Errorf("host %s does not have a known key", hostname)
				}

				if !bytes.Equal(targetKey.Marshal(), key.Marshal()) {
					return fmt.Errorf("host %s's key doesn't match the known key", hostname)
				}
			}

			return nil
		},
	})

	if err != nil {
		return nil, err
	}

	return client, nil
}

// Install installs the certificate
func (i *SFTPInstaller) Install(ctx context.Context, cert *convert.PemCertChain) error {
	var client *ssh.Client
	err := util.NewBackoff().WithCancel().WithLogger(func(err error, d time.Duration) {
		log.Printf(
			"encountered the following connection error whilst trying to install %s via sftp: %v, backing off for %v",
			cert.Domain,
			err,
			d,
		)
	}).Run(func() error {
		var err error
		client, err = i.dialClient()
		return err
	})
	if err != nil {
		return err
	}
	defer client.Close()

	for _, hook := range i.PreCopyHooks {
		err = util.NewBackoff().WithCancel().WithLogger(func(err error, d time.Duration) {
			log.Printf(
				"encountered the following pre-hook error whilst trying to install %s via sftp: %v, backing off for %v",
				cert.Domain,
				err,
				d,
			)
		}).Run(func() error {
			return hook.RunRemote(client)
		})

		if err != nil {
			return err
		}
	}

	err = util.NewBackoff().WithCancel().WithLogger(func(err error, d time.Duration) {
		log.Printf(
			"encountered the following sftp error whilst trying to install %s via sftp: %v, backing off for %v",
			cert.Domain,
			err,
			d,
		)
	}).Run(func() error {
		sftpClient, err := sftp.NewClient(client)
		if err != nil {
			return err
		}
		defer sftpClient.Close()

		for path, config := range i.Files {
			content := config.Render(cert)

			file, err := sftpClient.Create(path)
			if err != nil {
				return err
			}
			defer file.Close()

			err = file.Chmod(config.Mode)
			if err != nil {
				return err
			}

			_, err = fmt.Fprint(file, content)

			if err != nil {
				return err
			}
		}

		return nil
	})

	if err != nil {
		return err
	}

	for _, hook := range i.PostCopyHooks {
		err = util.NewBackoff().WithCancel().WithLogger(func(err error, d time.Duration) {
			log.Printf(
				"encountered the following post-hook error whilst trying to install %s via sftp: %v, backing off for %v",
				cert.Domain,
				err,
				d,
			)
		}).Run(func() error {
			return hook.RunRemote(client)
		})

		if err != nil {
			return err
		}
	}

	return nil
}

func (i *SFTPInstaller) parseKnownHosts(bytes []byte) error {
	for {
		marker, hosts, key, _, newBytes, err := ssh.ParseKnownHosts(bytes)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				return err
			}
		}
		bytes = newBytes

		if marker == "revoked" {
			continue
		}

		for j := 0; j < len(hosts); j++ {
			i.knownHosts[hosts[j]] = key
		}
	}

	return nil
}
