package installer

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/tcnj/acme-manager/convert"
	"gitlab.com/tcnj/acme-manager/hook"
	"gitlab.com/tcnj/acme-manager/util"
)

// LocalInstaller installs a certificate locally
type LocalInstaller struct {
	Files map[string]*convert.CertFileConfig `json:"files"`

	PreCopyHooks  []hook.Hook `json:"pre_copy_hooks"`
	PostCopyHooks []hook.Hook `json:"post_copy_hooks"`
}

// NewLocal creates a new Local installer
func NewLocal(settings json.RawMessage) (*LocalInstaller, error) {
	var i LocalInstaller

	err := json.Unmarshal(settings, &i)
	if err != nil {
		return nil, err
	}

	for _, config := range i.Files {
		err = config.Validate()
		if err != nil {
			return nil, err
		}
	}

	if len(i.Files) == 0 {
		return nil, fmt.Errorf("expected at least one file to write")
	}

	return &i, nil
}

// Install installs the certificate
func (i *LocalInstaller) Install(ctx context.Context, cert *convert.PemCertChain) error {
	for _, hook := range i.PreCopyHooks {
		err := util.NewBackoff().WithCancel().WithLogger(func(err error, d time.Duration) {
			log.Printf(
				"encountered the following pre-hook error whilst trying to install %s locally: %v, backing off for %v",
				cert.Domain,
				err,
				d,
			)
		}).Run(func() error {
			return hook.RunLocally()
		})

		if err != nil {
			return err
		}
	}

	err := util.NewBackoff().WithCancel().WithLogger(func(err error, d time.Duration) {
		log.Printf(
			"encountered the following error whilst trying to install %s locally: %v, backing off for %v",
			cert.Domain,
			err,
			d,
		)
	}).Run(func() error {
		for path, config := range i.Files {
			content := config.Render(cert)

			file, err := os.Create(path)
			if err != nil {
				return err
			}
			defer file.Close()

			err = file.Chmod(config.Mode)
			if err != nil {
				return err
			}

			_, err = fmt.Fprint(file, content)

			if err != nil {
				return err
			}
		}

		return nil
	})

	if err != nil {
		return err
	}

	for _, hook := range i.PostCopyHooks {
		err := util.NewBackoff().WithCancel().WithLogger(func(err error, d time.Duration) {
			log.Printf(
				"encountered the following post-hook error whilst trying to install %s locally: %v, backing off for %v",
				cert.Domain,
				err,
				d,
			)
		}).Run(func() error {
			return hook.RunLocally()
		})

		if err != nil {
			return err
		}
	}

	return nil
}
