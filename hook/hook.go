package hook

import (
	"os/exec"

	"golang.org/x/crypto/ssh"
)

// Hook represents a configurable hookpoint
type Hook struct {
	Command string `json:"command"`
}

// RunRemote runs this hook on the given SSH client
func (h *Hook) RunRemote(client *ssh.Client) error {
	session, err := client.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()

	err = session.Run(h.Command)

	return err
}

// RunLocally runs this hook locally
func (h *Hook) RunLocally() error {
	return exec.Command("/bin/sh", "-c", h.Command).Run()
}
