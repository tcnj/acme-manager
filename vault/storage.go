package vault

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/caddyserver/certmagic"
	"github.com/google/uuid"
	"github.com/hashicorp/vault/api"
)

const (
	lockExpiration = time.Minute * 30
	backoffLimit   = 16
)

// Storage implements a Storage for Vault
type Storage struct {
	client *api.Client
	kvPath string

	instanceID uuid.UUID
}

// NewStorage creates a new Storage
func NewStorage(client *api.Client, kvPath string) (*Storage, error) {
	kvPath = strings.TrimPrefix(
		strings.TrimSuffix(kvPath, "/"),
		"/",
	)

	if kvPath == "" {
		return nil, fmt.Errorf("expected kv_engine_path")
	}

	mounts, err := client.Sys().ListMounts()
	if err != nil {
		return nil, err
	}

	mountInfo, ok := mounts[strings.Join([]string{kvPath, "/"}, "")]
	if !ok {
		return nil, fmt.Errorf("vault does not have a mount at %s", kvPath)
	}

	if mountInfo.Type != "kv" {
		return nil, fmt.Errorf("given mountpoint is not of type kv")
	}

	id, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}

	return &Storage{
		client:     client,
		kvPath:     kvPath,
		instanceID: id,
	}, nil
}

// Lock blocks until the given key is locked
func (s *Storage) Lock(ctx context.Context, key string) error {
	log.Printf("locking %s...", key)
	for {
		version, err := s.waitLock(key)
		if err != nil {
			return err
		}

		log.Printf("trying for %s...", key)
		err = s.tryLock(key, version)
		if err == nil {
			log.Printf("got lock on %s", key)
			return nil
		}
	}
}

func (s *Storage) lockPathFor(key string) string {
	return strings.Join([]string{s.kvPath, "data", "lock", key}, "/")
}

func (s *Storage) waitLock(key string) (int64, error) {
	backoff := 1
	for {
		log.Printf("waiting for %s...", key)
		secret, err := s.client.Logical().Read(s.lockPathFor(key))
		if err != nil {
			return 0, err
		}

		if secret == nil {
			return 0, nil
		}

		metadata, ok := secret.Data["metadata"].(map[string]interface{})
		if !ok {
			return 0, fmt.Errorf("expected vault to return 'metadata' key")
		}

		versionJSON, ok := metadata["version"].(json.Number)
		if !ok {
			return 0, fmt.Errorf("expected vault to return secret version")
		}
		version, err := versionJSON.Int64()
		if err != nil {
			return 0, err
		}

		setTimeString, ok := metadata["created_time"].(string)
		if !ok {
			return 0, fmt.Errorf("expected vault to return secret created_time")
		}

		setTime, err := time.Parse(time.RFC3339, setTimeString)
		if err != nil {
			return 0, err
		}

		data, ok := secret.Data["data"].(map[string]interface{})
		if !ok {
			return 0, fmt.Errorf("expected vault to return secret data")
		}

		status, ok := data["lock"].(string)
		if !ok {
			return 0, fmt.Errorf("expected data to contain a lock key")
		}

		if status != "locked" || time.Now().Sub(setTime) > lockExpiration {
			return version, nil
		}

		time.Sleep(time.Duration(backoff) * time.Second)
		if backoff < backoffLimit {
			backoff = backoff * 2
		}
	}
}

func (s *Storage) tryLock(key string, version int64) error {
	_, err := s.client.Logical().Write(s.lockPathFor(key), map[string]interface{}{
		"options": map[string]interface{}{
			"cas": version,
		},
		"data": map[string]interface{}{
			"lock": "locked",
			"by":   s.instanceID.String(),
		},
	})
	return err
}

// Unlock unlocks the given key, if it is locked by this instance
func (s *Storage) Unlock(key string) error {
	secret, err := s.client.Logical().Read(s.lockPathFor(key))
	if err != nil {
		return err
	}

	if secret == nil {
		return nil
	}

	metadata, ok := secret.Data["metadata"].(map[string]interface{})
	if !ok {
		return fmt.Errorf("expected vault to return 'metadata' key")
	}

	versionJSON, ok := metadata["version"].(json.Number)
	if !ok {
		return fmt.Errorf("expected vault to return secret version")
	}
	version, err := versionJSON.Int64()
	if err != nil {
		return err
	}

	data, ok := secret.Data["data"].(map[string]interface{})
	if !ok {
		return fmt.Errorf("expected vault to return secret data")
	}

	status, ok := data["lock"].(string)
	if !ok {
		return fmt.Errorf("expected data to contain a lock key")
	}

	if status != "locked" {
		return nil
	}

	by, ok := data["by"].(string)
	if !ok {
		return fmt.Errorf("expected data to contain a by key")
	}

	byID, err := uuid.Parse(by)
	if err != nil {
		return err
	}

	if byID != s.instanceID {
		return nil
	}

	secret, err = s.client.Logical().Write(s.lockPathFor(key), map[string]interface{}{
		"options": map[string]interface{}{
			"cas": version,
		},
		"data": map[string]interface{}{
			"lock": "unlocked",
			"by":   s.instanceID.String(),
		},
	})

	if err != nil && strings.Contains(err.Error(), "check-and-set") {
		return nil
	}

	return err
}

func (s *Storage) dataPathFor(key string) string {
	return strings.Join([]string{s.kvPath, "data", "storage", key}, "/")
}

// Store stores the given value at key
func (s *Storage) Store(key string, value []byte) error {
	valueBase64 := base64.RawStdEncoding.EncodeToString(value)

	_, err := s.client.Logical().Write(s.dataPathFor(key), map[string]interface{}{
		"data": map[string]interface{}{
			"value": valueBase64,
		},
	})

	return err
}

// Load loads the given key's value. If the key does not exist, an error is returned.
func (s *Storage) Load(key string) ([]byte, error) {
	secret, err := s.client.Logical().Read(s.dataPathFor(key))
	if err != nil {
		return nil, fmt.Errorf("fetching key %s: %v", key, err)
	}
	// log.Printf("Got secret for key %s: %+v", key, secret)

	if secret == nil {
		return nil, fmt.Errorf("key %s not found", key)
	}

	data, ok := secret.Data["data"].(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("expected vault to return secret data")
	}

	valueBase64, ok := data["value"].(string)
	if !ok {
		return nil, fmt.Errorf("expected data to contain a value key")
	}

	value, err := base64.RawStdEncoding.DecodeString(valueBase64)
	if err != nil {
		return nil, err
	}

	return value, nil
}

// Delete deletes the given key
func (s *Storage) Delete(key string) error {
	_, err := s.client.Logical().Delete(s.dataPathFor(key))
	return err
}

// Exists checks that a key definitely exists
func (s *Storage) Exists(key string) bool {
	secret, err := s.client.Logical().Read(s.dataPathFor(key))
	if err != nil {
		return false
	}

	if secret == nil {
		return false
	}

	return true
}

func (s *Storage) dataMetaPathFor(key string) string {
	return strings.Join([]string{s.kvPath, "metadata", "storage", key}, "/")
}

// List allows (optionally recursive) listing of keys
func (s *Storage) List(prefix string, recursive bool) ([]string, error) {
	prefixesLeft := []string{prefix}
	results := []string{}

	for len(prefixesLeft) > 0 {
		currentPrefix := prefixesLeft[0]
		prefixesLeft = prefixesLeft[1:]

		secret, err := s.client.Logical().List(s.dataMetaPathFor(currentPrefix))
		if err != nil {
			return nil, err
		}

		if secret == nil {
			return nil, fmt.Errorf("empty response for list request")
		}

		keys, ok := secret.Data["keys"].([]string)
		if !ok {
			return nil, fmt.Errorf("expected vault to return keys data")
		}

		for i := 0; i < len(keys); i++ {
			key := keys[i]
			if strings.HasSuffix(key, "/") {
				if recursive {
					prefixesLeft = append(prefixesLeft, strings.Join(
						[]string{
							strings.TrimSuffix(prefix, "/"),
							strings.TrimSuffix(key, "/"),
						},
						"/",
					))
				}
			} else {
				results = append(results, strings.Join(
					[]string{
						strings.TrimSuffix(prefix, "/"),
						key,
					},
					"/",
				))
			}
		}
	}
	return results, nil
}

// Stat returns information about the given key
func (s *Storage) Stat(key string) (certmagic.KeyInfo, error) {
	target := s.dataMetaPathFor(key)

	secret, err := s.client.Logical().Read(target)
	if err != nil {
		return certmagic.KeyInfo{}, fmt.Errorf("fetching key metadata %s: %v", key, err)
	}

	if secret == nil {
		secret, err = s.client.Logical().List(target)
		if err != nil {
			return certmagic.KeyInfo{}, fmt.Errorf("fetching key list metadata %s: %v", key, err)
		}

		if secret != nil {
			return certmagic.KeyInfo{
				Key:        key,
				IsTerminal: false,
			}, nil
		}

		return certmagic.KeyInfo{}, fmt.Errorf("attempted to stat non-existant key")
	}

	return certmagic.KeyInfo{
		Key:        key,
		IsTerminal: true,
	}, nil
}
