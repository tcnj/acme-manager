package vault

import (
	"context"

	"github.com/hashicorp/vault/api"
)

type clientKey int

// WrapContext wraps a context with a vault client
func WrapContext(ctx context.Context, client *api.Client) context.Context {
	return context.WithValue(ctx, clientKey(0), client)
}

// FromContext produces a vault client from a context
func FromContext(ctx context.Context) *api.Client {
	c, ok := ctx.Value(clientKey(0)).(*api.Client)
	if !ok {
		panic("tried to take a vault client from a context where one didn't exist")
	}
	return c
}
