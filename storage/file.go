package storage

import (
	"encoding/json"

	"github.com/caddyserver/certmagic"
)

type fileConfig struct {
	path string
}

// NewFile creates a new certmagic.FileStorage
func NewFile(settings json.RawMessage) (certmagic.Storage, error) {
	var c fileConfig

	c.path = ".file-storage/"

	err := json.Unmarshal(settings, &c)
	if err != nil {
		return nil, err
	}

	return &certmagic.FileStorage{Path: c.path}, nil
}
