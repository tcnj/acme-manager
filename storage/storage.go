package storage

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/caddyserver/certmagic"
)

// Spec represents the config of a pluggable storage backend
type Spec struct {
	Type string `json:"type"`

	Settings json.RawMessage `json:"settings"`
}

// FromSpec produces a certmagic.Storage from a Spec
func FromSpec(ctx context.Context, spec *Spec) (certmagic.Storage, error) {
	var err error
	var storage certmagic.Storage

	switch spec.Type {
	case "file":
		storage, err = NewFile(spec.Settings)
	case "vault":
		storage, err = NewVault(ctx, spec.Settings)
	default:
		return nil, fmt.Errorf("unknown storage type %s", spec.Type)
	}

	if err != nil {
		return nil, err
	}

	return storage, nil
}
