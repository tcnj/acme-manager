package storage

import (
	"context"
	"encoding/json"

	"github.com/caddyserver/certmagic"
	"gitlab.com/tcnj/acme-manager/vault"
)

type vaultConfig struct {
	KVEnginePath string `json:"kv_engine_path"`
}

// NewVault creates a new vault.Storage
func NewVault(ctx context.Context, settings json.RawMessage) (certmagic.Storage, error) {
	var c vaultConfig

	err := json.Unmarshal(settings, &c)
	if err != nil {
		return nil, err
	}

	client := vault.FromContext(ctx)

	storage, err := vault.NewStorage(client, c.KVEnginePath)
	if err != nil {
		return nil, err
	}

	return storage, nil
}
