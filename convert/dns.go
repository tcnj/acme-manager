package convert

import "strings"

// CanonicaliseDNSName converts name into a more canonical form
func CanonicaliseDNSName(name string) string {
	return strings.TrimSuffix(strings.TrimPrefix(strings.ToLower(name), "."), ".")
}
