package convert

import (
	"crypto"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"
	"strings"
)

// PemCertChain represents a PEM-encoded chain, certificate and key
type PemCertChain struct {
	Chain  string `json:"chain"`
	Cert   string `json:"cert"`
	Key    string `json:"key"`
	Domain string `json:"domain"`

	chainSlice []string
}

// TLSToPem converts a tls.Certificate to a PEM-encoded chain, certificate and key
func TLSToPem(name string, cert *tls.Certificate) (*PemCertChain, error) {
	certBytes := cert.Certificate[0]
	cert.Certificate = cert.Certificate[1:]

	certBuilder := strings.Builder{}
	err := pem.Encode(&certBuilder, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: certBytes,
	})
	if err != nil {
		return nil, err
	}

	chain := []string{}
	totalCerts := len(cert.Certificate)
	for i := 0; i < totalCerts; i++ {
		currentCertBytes := cert.Certificate[totalCerts-i-1]

		certBuilder := strings.Builder{}
		err := pem.Encode(&certBuilder, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: currentCertBytes,
		})
		if err != nil {
			return nil, err
		}
		chain = append(chain, certBuilder.String())
	}

	privateKeyPem, err := privateKeyToPEM(cert.PrivateKey)
	if err != nil {
		return nil, err
	}

	return &PemCertChain{
		Chain:      strings.Join(chain, ""),
		Cert:       certBuilder.String(),
		Key:        privateKeyPem,
		Domain:     name,
		chainSlice: chain,
	}, nil
}

func privateKeyToPEM(key crypto.PrivateKey) (string, error) {
	bytes, err := x509.MarshalPKCS8PrivateKey(key)
	if err != nil {
		return "", err
	}

	builder := strings.Builder{}
	err = pem.Encode(&builder, &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: bytes,
	})
	if err != nil {
		return "", err
	}

	return builder.String(), nil
}

// CertFileConfig represents a desired sequence of the PEM-encoded parts of a PemCertChain
type CertFileConfig struct {
	Parts []string    `json:"parts"`
	Mode  os.FileMode `json:"mode"`
}

const (
	partIntermediate         = "intermediates"
	partIntermediateInverted = "intermediates-reversed"
	partCert                 = "cert"
	partKey                  = "key"
)

// Validate validates the CertFileConfig and returns any errors
func (c *CertFileConfig) Validate() error {
	for i := 0; i < len(c.Parts); i++ {
		part := c.Parts[i]

		switch part {
		case partIntermediate:
		case partCert:
		case partKey:
		case partIntermediateInverted:
		default:
			return fmt.Errorf("unknown certificate file part %s", part)
		}
	}

	if c.Mode == 0 {
		c.Mode = 0600
	}

	return nil
}

// Render renders a PemCertChain according to this CertFileConfig.
func (c *CertFileConfig) Render(cert *PemCertChain) string {
	buffer := ""

	for i := 0; i < len(c.Parts); i++ {
		switch c.Parts[i] {
		case partIntermediate:
			buffer += cert.Chain
		case partCert:
			buffer += cert.Cert
		case partKey:
			buffer += cert.Key
		case partIntermediateInverted:
			certs := len(cert.chainSlice)
			for j := 0; j < certs; j++ {
				buffer += cert.chainSlice[certs-j-1]
			}
		default:
		}
	}

	return buffer
}
