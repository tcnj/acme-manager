package util

import (
	"time"
)

// Backoff represents a configuration for repeatedly running a function with exponential backoff.
type Backoff struct {
	exponent float32
	limit    time.Duration
	initial  time.Duration

	log           func(error, time.Duration)
	cancelOnLimit bool
}

// NewBackoff creates a Backoff with sane defaults.
func NewBackoff() *Backoff {
	return &Backoff{
		exponent:      2,
		limit:         64 * time.Second,
		initial:       time.Second,
		log:           func(err error, d time.Duration) {},
		cancelOnLimit: false,
	}
}

// WithExponent sets the exponent of this Backoff
func (b *Backoff) WithExponent(exp float32) *Backoff {
	if exp >= 1 {
		b.exponent = exp
	}
	return b
}

// WithLimit sets the limit of this Backoff
func (b *Backoff) WithLimit(limit time.Duration) *Backoff {
	b.limit = limit
	return b
}

// WithInitial sets the initial backoff of this Backoff
func (b *Backoff) WithInitial(initial time.Duration) *Backoff {
	if initial > 0 {
		b.initial = initial
	}
	return b
}

// WithLogger adds a logging callback to this Backoff
func (b *Backoff) WithLogger(log func(error, time.Duration)) *Backoff {
	b.log = log
	return b
}

// WithCancel configures this Backoff to give up when it reaches the configured limit
func (b *Backoff) WithCancel() *Backoff {
	b.cancelOnLimit = true
	return b
}

// Run runs the given callback with exponential backoff, as configured by this Backoff
func (b *Backoff) Run(cb func() error) error {
	t := b.initial

	for {
		err := cb()

		if err == nil {
			return nil
		}

		b.log(err, t)

		time.Sleep(t)

		if t == b.limit && b.cancelOnLimit {
			return err
		}

		t = time.Duration(float64(t) * float64(b.exponent))
		if t > b.limit {
			t = b.limit
		}
	}
}
